%
% Master 0, Slave 1, "  "
%
function rv = slave4()

% Slave configuration

rv.SlaveConfig.vendor = 43690;
rv.SlaveConfig.product = hex2dec('00000001');
rv.SlaveConfig.description = 'D1-N-36-E0-2-1-00';
%rv.SlaveConfig.sdo = { hex2dec('1c12'),0,8,0; 		% set the 0x1c12 to zero to allow us to map new PDOs to the SyncManagers
%	    hex2dec('1c13'),0,8,0; 			% set the 0x1c13 to zero to allow us to map new PDOs to the SyncManagers
%	    hex2dec('1a00'),0,8,0; 			% clear PDO 0x1a00 entries (TxPDOs)
%	    hex2dec('1a00'),1,32,hex2dec('60410010'); 	% download StatusWord to mapping entry 1 of TxPDOs
%	    hex2dec('1a00'),2,32,hex2dec('60640020'); 	% download Position Actual Value to mapping entry 2 of TxPDOs
%	    hex2dec('1a00'),3,32,hex2dec('606C0020'); 	% download Velocity Actual Value to mapping entry 3 of TxPDOs
%	    hex2dec('1a00'),4,32,hex2dec('60610008'); 	% download Mode of Operation Display to mapping entry 4 of TxPDOs
%	    hex2dec('1a00'),0,8,3; 			% download number of entries in 0x1a00
%	    hex2dec('1600'),0,8,0; 			% clear PDO 0x1600 entries (RxPDOs)
%	    hex2dec('1600'),1,32,hex2dec('60400010'); 	% download ControlWord to mapping entry 1 of RxPDOs
%	    hex2dec('1600'),2,32,hex2dec('607a0020'); 	% download Target Position to mapping entry 2 of RxPDOs
%	    hex2dec('1600'),3,32,hex2dec('60710020'); 	% download Target Torque to mapping entry 3 of RxPDOs
%	    hex2dec('1600'),0,8,3; 			% download number of entries in 0x1600
%	    hex2dec('1c12'),1,16,hex2dec('1600'); 	% download pdo 0x1c12:01 index
%	    hex2dec('1c12'),0,8,1; 			% download pdo 0x1c12 count
%	    hex2dec('1c13'),1,16,hex2dec('1a00'); 	% download pdo 0x1c13:01 index
%	    hex2dec('1c13'),0,8,1; 			% download pdo 0x1c13 count
%	    hex2dec('6060'),0,8,10}; 			% download mode of operation value = 10
			
rv.SlaveConfig.sm = { ...
    {0, 0, {
        }}, ...
    {1, 1, {
        }}, ...
    {2, 0, {
        {hex2dec('1600'), [
	    hex2dec('6040'), hex2dec('00'),  16; ... % ControlWord (uint16)
%	    hex2dec('607a'), hex2dec('00'),  32; ... % Target Position (int32)    
	    hex2dec('6071'), hex2dec('00'),  16; ... % Target Torque (int16)
            ]}, ...
        }}, ...
    {3, 1, {
        {hex2dec('1a00'), [
            hex2dec('6041'), hex2dec('00'),  16; ... % StatusWord (uint16)
	    hex2dec('6077'), hex2dec('00'),  16; ... % Torque Actual Value (int16)
	    hex2dec('6064'), hex2dec('00'),  32; ... % Position Actual Value (int32)
%	    hex2dec('606C'), hex2dec('00'),  32; ... % Velocity Actual Value (int32)
	    hex2dec('2100'), hex2dec('00'),  16; ... % Error Code (uint16)
            ]}, ...
        }}, ...
    };

% Port configuration
% Data types
uint8 = 1008;
uint16 = 1016;
uint32 = 1032;
int8 = 2008;
int16 = 2016;
int32 = 2032;

% Model inputs (Master>Slave)
rv.PortConfig.input(1).pdo = [2, 0, 0, 0]; % ControlWord 6040
rv.PortConfig.input(1).pdo_data_type = uint16;

%rv.PortConfig.input(2).pdo = [2, 0, 1, 0]; % Target Position 607a
%rv.PortConfig.input(2).pdo_data_type = int32;

rv.PortConfig.input(3).pdo = [2, 0, 1, 0]; % Target Torque 6071
rv.PortConfig.input(3).pdo_data_type = int16;


% Model output (Slave>Master)
rv.PortConfig.output(1).pdo = [3, 0, 0, 0]; % StatusWord 6041
rv.PortConfig.output(1).pdo_data_type = uint16;

rv.PortConfig.output(2).pdo = [3, 0, 1, 0]; % Torque Actual Value 606C
rv.PortConfig.output(2).pdo_data_type = int16;

rv.PortConfig.output(3).pdo = [3, 0, 2, 0]; % Position Actual Value 6064
rv.PortConfig.output(3).pdo_data_type = int32;

%rv.PortConfig.output(3).pdo = [3, 0, 2, 0]; % Velocity Actual Value 606C
%rv.PortConfig.output(3).pdo_data_type = int32;

rv.PortConfig.output(4).pdo = [3, 0, 3, 0]; % Error Code 603F
rv.PortConfig.output(4).pdo_data_type = uint16;
end
