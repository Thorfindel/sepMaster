% load ref.mat into workspace, allowing it to be run
load('/home/sep/softpro/softproRepo/ref.mat'); 


% set minimum position
initMin = get_param([bdroot '/minPosLim'],'value');
set_param([bdroot '/settingLimits/soh1'],'initCond',num2str(initMin));

% set maximum position
initMax = get_param([bdroot '/maxPosLim'],'value');
set_param([bdroot '/settingLimits/soh2'],'initCond',num2str(initMax));