function newStatus = cleanStatus(status,bitNum)
%cleanStatus allows you to set unneeded bit values to 0. 
% newStatus = the end result
% status = the bitline you want to change
% bitNum = the bit you want to change, assuming a zero-based binary number!
newStatus = uint16(bitset(status,bitNum+1,0));
end