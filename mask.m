function result = mask(input,maskBits)
%mask will apply the BITAND operation and return the decimal value
% currently only works for uint16 type for the SEP
tmp = uint16(bin2dec(maskBits));
result = (bitand(input,tmp));
end

