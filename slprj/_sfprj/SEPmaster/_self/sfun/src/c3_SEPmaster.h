#ifndef __c3_SEPmaster_h__
#define __c3_SEPmaster_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc3_SEPmasterInstanceStruct
#define typedef_SFc3_SEPmasterInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c3_sfEvent;
  boolean_T c3_isStable;
  boolean_T c3_doneDoubleBufferReInit;
  uint8_T c3_is_active_c3_SEPmaster;
  real_T c3_timer;
  boolean_T c3_timer_not_empty;
  real_T c3_midpoint;
  boolean_T c3_midpoint_not_empty;
  real_T c3_kvirtual;
  boolean_T c3_kvirtual_not_empty;
  real_T *c3_currentPos;
  real_T *c3_start;
  real_T *c3_chosenRef;
} SFc3_SEPmasterInstanceStruct;

#endif                                 /*typedef_SFc3_SEPmasterInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_SEPmaster_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_SEPmaster_get_check_sum(mxArray *plhs[]);
extern void c3_SEPmaster_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
