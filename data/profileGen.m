tMax = 4*pi; %[s]
nMax = floor(1000*tMax); %[-]
tArray = linspace(0,tMax,nMax); %[s]
nArray = 1:nMax; %[-]
rTorque = 100*square(tArray);
rTorque(end) = 0;

Data = [tArray;nArray; rTorque];
% names = ['time','sampleNum','refTorque'];
fid = fopen('test_file.mat','w');
fprintf(fid,'%6s\t%9s\t%9s \n','time','sample','refTorque');
fprintf(fid,'%6.3f\t%9.0f\t%0.0f \n',Data);
fclose(fid);